(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/api/api.types.ts":
/*!**********************************!*\
  !*** ./src/app/api/api.types.ts ***!
  \**********************************/
/*! exports provided: ImageEntryState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageEntryState", function() { return ImageEntryState; });
var ImageEntryState;
(function (ImageEntryState) {
    ImageEntryState["AWAITING_REVIEW"] = "AWAITING_REVIEW";
    ImageEntryState["APPROVED"] = "APPROVED";
    ImageEntryState["REJECTED"] = "REJECTED";
})(ImageEntryState || (ImageEntryState = {}));


/***/ }),

/***/ "./src/app/api/classes/image-entry.class.ts":
/*!**************************************************!*\
  !*** ./src/app/api/classes/image-entry.class.ts ***!
  \**************************************************/
/*! exports provided: ImageEntry */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageEntry", function() { return ImageEntry; });
/* harmony import */ var _api_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../api.types */ "./src/app/api/api.types.ts");

var ImageEntry = /** @class */ (function () {
    function ImageEntry(imageEntryDto) {
        this.initialiseFromDto(imageEntryDto);
    }
    ImageEntry.prototype.isPending = function () {
        return this.approvalState === _api_types__WEBPACK_IMPORTED_MODULE_0__["ImageEntryState"].AWAITING_REVIEW;
    };
    ImageEntry.prototype.isApproved = function () {
        return this.approvalState === _api_types__WEBPACK_IMPORTED_MODULE_0__["ImageEntryState"].APPROVED;
    };
    ImageEntry.prototype.isRejected = function () {
        return this.approvalState === _api_types__WEBPACK_IMPORTED_MODULE_0__["ImageEntryState"].REJECTED;
    };
    ImageEntry.prototype.copy = function () {
        var newObj = Object.create(Object.getPrototypeOf(this));
        return Object.assign(newObj, this);
    };
    ImageEntry.prototype.toDto = function () {
        return Object.assign({}, this);
    };
    ImageEntry.prototype.initialiseFromDto = function (dto) {
        Object.assign(this, dto);
    };
    return ImageEntry;
}());



/***/ }),

/***/ "./src/app/api/classes/school.class.ts":
/*!*********************************************!*\
  !*** ./src/app/api/classes/school.class.ts ***!
  \*********************************************/
/*! exports provided: School */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "School", function() { return School; });
var School = /** @class */ (function () {
    function School(schoolDto) {
        this.initialiseFromDto(schoolDto);
    }
    School.prototype.copy = function () {
        var newObj = Object.create(Object.getPrototypeOf(this));
        return Object.assign(newObj, this);
    };
    School.prototype.toDto = function () {
        return Object.assign({}, this);
    };
    School.prototype.initialiseFromDto = function (dto) {
        Object.assign(this, dto);
    };
    return School;
}());



/***/ }),

/***/ "./src/app/api/classes/user.class.ts":
/*!*******************************************!*\
  !*** ./src/app/api/classes/user.class.ts ***!
  \*******************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/* harmony import */ var _image_entry_class__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./image-entry.class */ "./src/app/api/classes/image-entry.class.ts");

var User = /** @class */ (function () {
    function User(userDto) {
        this.initialiseFromDto(userDto);
    }
    User.prototype.getPending = function () {
        return this.imageEntryList.filter(function (imageEntry) { return imageEntry.isPending(); });
    };
    User.prototype.getApproved = function () {
        return this.imageEntryList.filter(function (imageEntry) { return imageEntry.isApproved(); });
    };
    User.prototype.getRejected = function () {
        return this.imageEntryList.filter(function (imageEntry) { return imageEntry.isRejected(); });
    };
    User.prototype.getSubmitted = function () {
        return this.imageEntryList.length;
    };
    User.prototype.getFullName = function () {
        return this.firstName + " " + this.lastName;
    };
    User.prototype.copy = function () {
        var newObj = Object.create(Object.getPrototypeOf(this));
        return Object.assign(newObj, this);
    };
    User.prototype.toDto = function () {
        return Object.assign({}, this, {
            imageEntryList: this.imageEntryList.map(function (imageEntry) { return imageEntry.toDto(); })
        });
    };
    User.prototype.initialiseFromDto = function (dto) {
        Object.assign(this, dto, {
            imageEntryList: dto.imageEntryList.map(function (imageEntryDto) { return new _image_entry_class__WEBPACK_IMPORTED_MODULE_0__["ImageEntry"](imageEntryDto); })
        });
    };
    return User;
}());



/***/ }),

/***/ "./src/app/api/domain.types.ts":
/*!*************************************!*\
  !*** ./src/app/api/domain.types.ts ***!
  \*************************************/
/*! exports provided: School, User, ImageEntry */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classes_school_class__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes/school.class */ "./src/app/api/classes/school.class.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "School", function() { return _classes_school_class__WEBPACK_IMPORTED_MODULE_0__["School"]; });

/* harmony import */ var _classes_user_class__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./classes/user.class */ "./src/app/api/classes/user.class.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "User", function() { return _classes_user_class__WEBPACK_IMPORTED_MODULE_1__["User"]; });

/* harmony import */ var _classes_image_entry_class__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./classes/image-entry.class */ "./src/app/api/classes/image-entry.class.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ImageEntry", function() { return _classes_image_entry_class__WEBPACK_IMPORTED_MODULE_2__["ImageEntry"]; });







/***/ }),

/***/ "./src/app/api/firebase.service.ts":
/*!*****************************************!*\
  !*** ./src/app/api/firebase.service.ts ***!
  \*****************************************/
/*! exports provided: FirebaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseService", function() { return FirebaseService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/database */ "./node_modules/angularfire2/database/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FirebaseService = /** @class */ (function () {
    function FirebaseService(db) {
        this.db = db;
        this.subscribeToSchools();
        this.subscribeToUsers();
    }
    FirebaseService.prototype.subscribeToUsers = function () {
        return this.usersReference().valueChanges();
    };
    FirebaseService.prototype.subscribeToSchools = function () {
        return this.schoolsReference().valueChanges();
    };
    FirebaseService.prototype.saveUser = function (user) {
        this.usersReference().set(user.id, user.toDto());
    };
    FirebaseService.prototype.usersReference = function () {
        return this.db.list('users');
    };
    FirebaseService.prototype.schoolsReference = function () {
        return this.db.list('schools');
    };
    FirebaseService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [angularfire2_database__WEBPACK_IMPORTED_MODULE_1__["AngularFireDatabase"]])
    ], FirebaseService);
    return FirebaseService;
}());



/***/ }),

/***/ "./src/app/api/schools.service.ts":
/*!****************************************!*\
  !*** ./src/app/api/schools.service.ts ***!
  \****************************************/
/*! exports provided: SchoolsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolsService", function() { return SchoolsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
/* harmony import */ var _firebase_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./firebase.service */ "./src/app/api/firebase.service.ts");
/* harmony import */ var _classes_school_class__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./classes/school.class */ "./src/app/api/classes/school.class.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SchoolsService = /** @class */ (function () {
    function SchoolsService(firebaseService, afAuth) {
        this.firebaseService = firebaseService;
        this.afAuth = afAuth;
    }
    SchoolsService.prototype.getSchool = function () {
        var schoolId = this.getActiveSchoolId();
        return this.firebaseService.subscribeToSchools().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (schoolDtoList) {
            return schoolDtoList
                .filter(function (schoolDto) { return schoolDto.key === schoolId; })
                .map(function (schoolDto) { return new _classes_school_class__WEBPACK_IMPORTED_MODULE_4__["School"](schoolDto); })
                .pop();
        }));
    };
    SchoolsService.prototype.getActiveSchoolId = function () {
        var uid = this.afAuth.auth.currentUser.uid;
        switch (uid) {
            case 'damhg61mXwWbbXXeI1vRy9RJg2K3':
                return 'school_one';
            case 'w0wpjsJY5mYSQlg2ce0hQSyVNOn1':
                return 'school_two';
            default:
                return 'school_two';
        }
    };
    SchoolsService.prototype.getSchoolName = function (key) {
        return this.schools[key].name;
    };
    SchoolsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_firebase_service__WEBPACK_IMPORTED_MODULE_3__["FirebaseService"], angularfire2_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"]])
    ], SchoolsService);
    return SchoolsService;
}());



/***/ }),

/***/ "./src/app/api/users.service.ts":
/*!**************************************!*\
  !*** ./src/app/api/users.service.ts ***!
  \**************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _firebase_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./firebase.service */ "./src/app/api/firebase.service.ts");
/* harmony import */ var _schools_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./schools.service */ "./src/app/api/schools.service.ts");
/* harmony import */ var _domain_types__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./domain.types */ "./src/app/api/domain.types.ts");
/* harmony import */ var _api_types__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./api.types */ "./src/app/api/api.types.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UsersService = /** @class */ (function () {
    function UsersService(firebaseService, schoolService) {
        this.firebaseService = firebaseService;
        this.schoolService = schoolService;
    }
    UsersService.prototype.getUsers = function () {
        var _this = this;
        return this.firebaseService.subscribeToUsers().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (users) {
            return users
                .map(function (userDto) { return new _domain_types__WEBPACK_IMPORTED_MODULE_4__["User"](userDto); })
                .filter(function (user) { return user.schoolId === _this.schoolService.getActiveSchoolId(); });
        }));
    };
    UsersService.prototype.approveUserImage = function (user, imageToApprove) {
        var approvedImage = imageToApprove.copy();
        approvedImage.approvalState = _api_types__WEBPACK_IMPORTED_MODULE_5__["ImageEntryState"].APPROVED;
        this.replaceImageForUser(user, approvedImage);
    };
    UsersService.prototype.rejectUserImage = function (user, imageToReject) {
        var rejectedImage = imageToReject.copy();
        rejectedImage.approvalState = _api_types__WEBPACK_IMPORTED_MODULE_5__["ImageEntryState"].REJECTED;
        this.replaceImageForUser(user, rejectedImage);
    };
    UsersService.prototype.replaceImageForUser = function (user, newImage) {
        var userToSave = user.copy();
        userToSave.imageEntryList = userToSave.imageEntryList.map(function (image) {
            if (image.key === newImage.key) {
                return newImage;
            }
            return image;
        });
        this.firebaseService.saveUser(userToSave);
    };
    UsersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_firebase_service__WEBPACK_IMPORTED_MODULE_2__["FirebaseService"], _schools_service__WEBPACK_IMPORTED_MODULE_3__["SchoolsService"]])
    ], UsersService);
    return UsersService;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".actions .btn {\r\n    float: right;\r\n    margin-left: 5px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n\r\n<app-navigation></app-navigation>\r\n<div class=\"container\">\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <router-outlet></router-outlet>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(afAuth, router) {
        this.afAuth = afAuth;
        this.router = router;
        this.title = 'edu';
        afAuth.auth.onAuthStateChanged(function (user) {
            if (user) {
                router.navigate(['/dashboard']);
            }
            else {
                router.navigate(['/login']);
            }
        });
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [angularfire2_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angularfire2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angularfire2 */ "./node_modules/angularfire2/index.js");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angularfire2/database */ "./node_modules/angularfire2/database/index.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _list_uploads_list_uploads_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./list-uploads/list-uploads.component */ "./src/app/list-uploads/list-uploads.component.ts");
/* harmony import */ var _api_firebase_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./api/firebase.service */ "./src/app/api/firebase.service.ts");
/* harmony import */ var _api_users_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./api/users.service */ "./src/app/api/users.service.ts");
/* harmony import */ var _api_schools_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./api/schools.service */ "./src/app/api/schools.service.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _schools_schools_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./schools/schools.component */ "./src/app/schools/schools.component.ts");
/* harmony import */ var _teachers_teachers_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./teachers/teachers.component */ "./src/app/teachers/teachers.component.ts");
/* harmony import */ var _teacher_teacher_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./teacher/teacher.component */ "./src/app/teacher/teacher.component.ts");
/* harmony import */ var _verification_verification_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./verification/verification.component */ "./src/app/verification/verification.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./navigation/navigation.component */ "./src/app/navigation/navigation.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















var appRoutes = [
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_21__["LoginComponent"] },
    { path: 'teachers', component: _teachers_teachers_component__WEBPACK_IMPORTED_MODULE_16__["TeachersComponent"], canActivate: [_auth_auth_service__WEBPACK_IMPORTED_MODULE_14__["AuthService"]] },
    { path: 'teacher/:id', component: _teacher_teacher_component__WEBPACK_IMPORTED_MODULE_17__["TeacherComponent"], canActivate: [_auth_auth_service__WEBPACK_IMPORTED_MODULE_14__["AuthService"]] },
    { path: 'verification', component: _verification_verification_component__WEBPACK_IMPORTED_MODULE_18__["VerificationComponent"], canActivate: [_auth_auth_service__WEBPACK_IMPORTED_MODULE_14__["AuthService"]] },
    { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_19__["DashboardComponent"], canActivate: [_auth_auth_service__WEBPACK_IMPORTED_MODULE_14__["AuthService"]] },
    { path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
                _list_uploads_list_uploads_component__WEBPACK_IMPORTED_MODULE_10__["ListUploadsComponent"],
                _schools_schools_component__WEBPACK_IMPORTED_MODULE_15__["SchoolsComponent"],
                _teachers_teachers_component__WEBPACK_IMPORTED_MODULE_16__["TeachersComponent"],
                _teacher_teacher_component__WEBPACK_IMPORTED_MODULE_17__["TeacherComponent"],
                _verification_verification_component__WEBPACK_IMPORTED_MODULE_18__["VerificationComponent"],
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_19__["DashboardComponent"],
                _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_20__["NavigationComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_21__["LoginComponent"]
            ],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(appRoutes),
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                angularfire2__WEBPACK_IMPORTED_MODULE_4__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].firebase, 'attendance'),
                angularfire2_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabaseModule"]
            ],
            providers: [
                _api_firebase_service__WEBPACK_IMPORTED_MODULE_11__["FirebaseService"],
                _api_users_service__WEBPACK_IMPORTED_MODULE_12__["UsersService"],
                _api_schools_service__WEBPACK_IMPORTED_MODULE_13__["SchoolsService"],
                angularfire2_auth__WEBPACK_IMPORTED_MODULE_6__["AngularFireAuth"],
                _auth_auth_service__WEBPACK_IMPORTED_MODULE_14__["AuthService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthService = /** @class */ (function () {
    function AuthService(afAuth) {
        this.afAuth = afAuth;
    }
    AuthService.prototype.canActivate = function () {
        return !!this.afAuth.auth.currentUser;
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [angularfire2_auth__WEBPACK_IMPORTED_MODULE_1__["AngularFireAuth"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dashboard-section {\r\n    margin-bottom: 20px;\r\n}"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 *ngIf=\"school && school.name\">{{ school.name }}:</h2>\n\n\n<h5>Usage Stats</h5>\n<section class=\"dashboard-section\">\n  <div class=\"row\">\n    <div class=\"col-6\">\n      <div class=\"card\">\n        <div class=\"card-body text-center\">\n          <h5 class=\"card-title\">Number of teachers</h5>\n          <h3 class=\"card-subtitle mb-2 \">{{ numTeachers }}</h3>\n\n          <a routerLink=\"/teachers\" class=\"card-link\">View all registered teachers</a>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-6\">\n      <div class=\"card\">\n        <div class=\"card-body text-center\">\n          <h5 class=\"card-title\">Pending verifications</h5>\n          <h3 class=\"card-subtitle mb-2 \">{{ numPending }}</h3>\n          <a routerLink=\"/verification\" class=\"card-link\">View pending images</a>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n\n\n<section class=\"dashboard-section\">\n  <div class=\"row\">\n    <div class=\"col-4\">\n      <div class=\"card\">\n        <div class=\"card-body text-center\">\n          <h5 class=\"card-title\">Total uploads</h5>\n          <h3 class=\"card-subtitle mb-2 \">{{ numImages }}</h3>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-4\">\n      <div class=\"card\">\n        <div class=\"card-body text-center\">\n          <h5 class=\"card-title\">Number approved</h5>\n          <h3 class=\"card-subtitle mb-2 \">{{ numApproved }}</h3>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-4\">\n      <div class=\"card\">\n        <div class=\"card-body text-center\">\n          <h5 class=\"card-title\">Number rejected</h5>\n          <h3 class=\"card-subtitle mb-2 \">{{ numRejected }}</h3>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../api/users.service */ "./src/app/api/users.service.ts");
/* harmony import */ var _api_schools_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/schools.service */ "./src/app/api/schools.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(usersService, schoolsService) {
        this.usersService = usersService;
        this.schoolsService = schoolsService;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.usersService.getUsers().subscribe(function (teachers) {
            _this.teachers = teachers;
            _this.setStats(teachers);
        });
        this.schoolsService.getSchool().subscribe(function (school) {
            _this.school = school;
        });
    };
    DashboardComponent.prototype.setStats = function (teachers) {
        var images = teachers.reduce(function (imageList, teacher) {
            return imageList.concat(teacher.imageEntryList);
        }, []);
        this.numImages = images.length;
        this.numTeachers = teachers.length;
        this.numPending = images.filter(function (image) { return image.isPending(); }).length;
        this.numApproved = images.filter(function (image) { return image.isApproved(); }).length;
        this.numRejected = images.filter(function (image) { return image.isRejected(); }).length;
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [_api_users_service__WEBPACK_IMPORTED_MODULE_1__["UsersService"], _api_schools_service__WEBPACK_IMPORTED_MODULE_2__["SchoolsService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/list-uploads/list-uploads.component.css":
/*!*********************************************************!*\
  !*** ./src/app/list-uploads/list-uploads.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/list-uploads/list-uploads.component.html":
/*!**********************************************************!*\
  !*** ./src/app/list-uploads/list-uploads.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  list-uploads works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/list-uploads/list-uploads.component.ts":
/*!********************************************************!*\
  !*** ./src/app/list-uploads/list-uploads.component.ts ***!
  \********************************************************/
/*! exports provided: ListUploadsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListUploadsComponent", function() { return ListUploadsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ListUploadsComponent = /** @class */ (function () {
    function ListUploadsComponent() {
    }
    ListUploadsComponent.prototype.ngOnInit = function () {
    };
    ListUploadsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-uploads',
            template: __webpack_require__(/*! ./list-uploads.component.html */ "./src/app/list-uploads/list-uploads.component.html"),
            styles: [__webpack_require__(/*! ./list-uploads.component.css */ "./src/app/list-uploads/list-uploads.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ListUploadsComponent);
    return ListUploadsComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row justify-content-center\">\n    <div class=\"col-6 card\">\n      <div class=\"card-body\">\n        <h5 class=\"card-title\">Log in</h5>\n        <form #loginForm=\"ngForm\" (ngSubmit)=\"onSubmit()\">\n          <div class=\"form-group\">\n            <label for=\"exampleInputEmail1\">Email address</label>\n            <input [(ngModel)]=\"model.email\" type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" name=\"email\" aria-describedby=\"emailHelp\" placeholder=\"Enter email\">\n          </div>\n          <div class=\"form-group\">\n            <label for=\"exampleInputPassword1\">Password</label>\n            <input [(ngModel)]=\"model.password\" type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\" name=\"password\" placeholder=\"Password\">\n          </div>\n          <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n        </form>\n      </div>\n      \n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoginComponent = /** @class */ (function () {
    function LoginComponent(auth) {
        this.auth = auth;
        this.model = { email: "", password: "" };
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.onSubmit = function () {
        this.auth.auth.signInWithEmailAndPassword(this.model.email, this.model.password);
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [angularfire2_auth__WEBPACK_IMPORTED_MODULE_1__["AngularFireAuth"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/navigation/navigation.component.css":
/*!*****************************************************!*\
  !*** ./src/app/navigation/navigation.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".app-nav {\r\n    margin-bottom: 20px;\r\n}\r\n\r\n.app-nav {\r\n    background-color: #7fbb85;\r\n    color: #fbfbfb;\r\n}\r\n\r\n.title {\r\n    color: #fbfbfb;\r\n}\r\n\r\n.nav-item.active {\r\n    background-color: #61ad68;\r\n}\r\n\r\n.nav-item {\r\n    color: #fff;\r\n    cursor: pointer;\r\n}"

/***/ }),

/***/ "./src/app/navigation/navigation.component.html":
/*!******************************************************!*\
  !*** ./src/app/navigation/navigation.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app-nav\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-2\">\n                <a href=\"\" class=\"title navbar-brand\">\n                    educam\n                </a>\n            </div>\n            <div class=\"col-10\">\n                <ul class=\"nav justify-content-end\">\n                    <li>\n                        <a routerLink=\"/dashboard\" routerLinkActive=\"active\" class=\"nav-item nav-link\">\n                            Dashboard\n                        </a>\n                    </li>\n                    <li>\n                        <a routerLink=\"/teachers\" routerLinkActive=\"active\" class=\"nav-item nav-link\">\n                            Teachers\n                        </a>\n                    </li>\n                    <li>\n                        <a routerLink=\"/verification\" routerLinkActive=\"active\" class=\"nav-item nav-link\">\n                            Verification\n                        </a>\n                    </li>\n                    \n                    <li ngbDropdown class=\"d-inline-block\">\n                      <a class=\"nav-item nav-link\" id=\"dropdownBasic2\" ngbDropdownToggle>Account</a>\n                      <div ngbDropdownMenu aria-labelledby=\"dropdownBasic2\">\n                        <button class=\"dropdown-item\" (click)=\"logout()\">Log out</button>\n                      </div>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/navigation/navigation.component.ts":
/*!****************************************************!*\
  !*** ./src/app/navigation/navigation.component.ts ***!
  \****************************************************/
/*! exports provided: NavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return NavigationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavigationComponent = /** @class */ (function () {
    function NavigationComponent(afAuth) {
        this.afAuth = afAuth;
    }
    NavigationComponent.prototype.ngOnInit = function () {
    };
    NavigationComponent.prototype.logout = function () {
        this.afAuth.auth.signOut();
    };
    NavigationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navigation',
            template: __webpack_require__(/*! ./navigation.component.html */ "./src/app/navigation/navigation.component.html"),
            styles: [__webpack_require__(/*! ./navigation.component.css */ "./src/app/navigation/navigation.component.css")]
        }),
        __metadata("design:paramtypes", [angularfire2_auth__WEBPACK_IMPORTED_MODULE_1__["AngularFireAuth"]])
    ], NavigationComponent);
    return NavigationComponent;
}());



/***/ }),

/***/ "./src/app/schools/schools.component.css":
/*!***********************************************!*\
  !*** ./src/app/schools/schools.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/schools/schools.component.html":
/*!************************************************!*\
  !*** ./src/app/schools/schools.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>Schools</h3>\n<ul class=\"list-unstyled\">\n  <li *ngFor=\"let school of schools\">\n    {{ school.name }}\n  </li>\n</ul>"

/***/ }),

/***/ "./src/app/schools/schools.component.ts":
/*!**********************************************!*\
  !*** ./src/app/schools/schools.component.ts ***!
  \**********************************************/
/*! exports provided: SchoolsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolsComponent", function() { return SchoolsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_schools_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../api/schools.service */ "./src/app/api/schools.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SchoolsComponent = /** @class */ (function () {
    function SchoolsComponent(schoolsService) {
        this.schoolsService = schoolsService;
    }
    SchoolsComponent.prototype.ngOnInit = function () {
    };
    SchoolsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-schools',
            template: __webpack_require__(/*! ./schools.component.html */ "./src/app/schools/schools.component.html"),
            styles: [__webpack_require__(/*! ./schools.component.css */ "./src/app/schools/schools.component.css")]
        }),
        __metadata("design:paramtypes", [_api_schools_service__WEBPACK_IMPORTED_MODULE_1__["SchoolsService"]])
    ], SchoolsComponent);
    return SchoolsComponent;
}());



/***/ }),

/***/ "./src/app/teacher/teacher.component.css":
/*!***********************************************!*\
  !*** ./src/app/teacher/teacher.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/teacher/teacher.component.html":
/*!************************************************!*\
  !*** ./src/app/teacher/teacher.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  teacher works!\n</p>\n"

/***/ }),

/***/ "./src/app/teacher/teacher.component.ts":
/*!**********************************************!*\
  !*** ./src/app/teacher/teacher.component.ts ***!
  \**********************************************/
/*! exports provided: TeacherComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeacherComponent", function() { return TeacherComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TeacherComponent = /** @class */ (function () {
    function TeacherComponent() {
    }
    TeacherComponent.prototype.ngOnInit = function () {
    };
    TeacherComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-teacher',
            template: __webpack_require__(/*! ./teacher.component.html */ "./src/app/teacher/teacher.component.html"),
            styles: [__webpack_require__(/*! ./teacher.component.css */ "./src/app/teacher/teacher.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TeacherComponent);
    return TeacherComponent;
}());



/***/ }),

/***/ "./src/app/teachers/teachers.component.css":
/*!*************************************************!*\
  !*** ./src/app/teachers/teachers.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".teachers-table {\r\n    background: #fff;\r\n    margin-top: 20px;\r\n}"

/***/ }),

/***/ "./src/app/teachers/teachers.component.html":
/*!**************************************************!*\
  !*** ./src/app/teachers/teachers.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>\n  Currently registered teachers\n</h2>\n<table class=\"table table-striped table-bordered teachers-table\">\n  <thead>\n    <tr>\n      <th>Teacher name</th>\n      <th>Number of pending images</th>\n      <th>Number submitted images</th>\n      <th>Approved</th>\n      <th></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let teacher of this.teachers\">\n      <td>{{ teacher.getFullName() }}</td>\n      <td>\n        {{ teacher.getPending().length }}\n      </td>\n      <td>\n        {{ teacher.getSubmitted() }}\n      </td>\n      <td>\n        {{ teacher.getApproved().length }}\n      </td>\n      <td>\n        <a href=\"\">View attendance</a>\n      </td>\n    </tr>\n  </tbody>\n</table>"

/***/ }),

/***/ "./src/app/teachers/teachers.component.ts":
/*!************************************************!*\
  !*** ./src/app/teachers/teachers.component.ts ***!
  \************************************************/
/*! exports provided: TeachersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeachersComponent", function() { return TeachersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../api/users.service */ "./src/app/api/users.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TeachersComponent = /** @class */ (function () {
    function TeachersComponent(usersService) {
        this.usersService = usersService;
    }
    TeachersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.usersService.getUsers().subscribe(function (users) {
            _this.teachers = users;
        });
    };
    TeachersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-teachers',
            template: __webpack_require__(/*! ./teachers.component.html */ "./src/app/teachers/teachers.component.html"),
            styles: [__webpack_require__(/*! ./teachers.component.css */ "./src/app/teachers/teachers.component.css")]
        }),
        __metadata("design:paramtypes", [_api_users_service__WEBPACK_IMPORTED_MODULE_1__["UsersService"]])
    ], TeachersComponent);
    return TeachersComponent;
}());



/***/ }),

/***/ "./src/app/verification/verification.component.css":
/*!*********************************************************!*\
  !*** ./src/app/verification/verification.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".empty-state {\r\n    text-align: center;\r\n\r\n}\r\n\r\n.verification-image {\r\n    margin-top: auto 20px;\r\n    max-width: 500px;\r\n}\r\n\r\n.verification-card {\r\n    margin-bottom: 20px;\r\n}\r\n\r\n.verification-actions {\r\n    margin-top: 20px;\r\n}\r\n\r\n.verification-actions .btn {\r\n    margin-right: 10px;\r\n}"

/***/ }),

/***/ "./src/app/verification/verification.component.html":
/*!**********************************************************!*\
  !*** ./src/app/verification/verification.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div *ngIf=\"pendingVerifications && pendingVerifications.length\">\n  <h4>\n    Images requiring verification\n  </h4>\n  <div class=\"verification-card\" *ngFor=\"let pending of pendingVerifications\">\n    <div class=\"card\">           \n      <div class=\"row\">\n        <div class=\"col-6\">\n          <img class=\"card-img-top verification-image img-thumbnail\" src=\"{{ pending.image.downloadUrl }}\" alt=\"\">\n        </div>\n        <div class=\"col-6\">\n           <div class=\"card-body\">\n             <h3>Uploaded by: {{ pending.teacher.getFullName() }}</h3>\n             <strong>Uploaded at: {{ pending.image.timestamp }}</strong>\n              <div class=\"actions clearfix verification-actions\">\n                  <button class=\"btn-success btn btn-sm\" (click)=\"approveImage(pending.teacher, pending.image)\">Approve </button>\n                  <button class=\"btn-danger btn btn-sm\" (click)=\"rejectImage(pending.teacher, pending.image)\">Reject</button>\n              </div>\n          </div>\n        </div>\n      </div>  \n      </div>\n  </div>\n\n</div>\n\n\n<div *ngIf=\"pendingVerifications && pendingVerifications.length === 0\">\n  <div class=\"alert alert-success\">\n    All images have been reviewed - well done!\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/verification/verification.component.ts":
/*!********************************************************!*\
  !*** ./src/app/verification/verification.component.ts ***!
  \********************************************************/
/*! exports provided: VerificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerificationComponent", function() { return VerificationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../api/users.service */ "./src/app/api/users.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var VerificationComponent = /** @class */ (function () {
    function VerificationComponent(usersService) {
        this.usersService = usersService;
    }
    VerificationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.usersService.getUsers().subscribe(function (users) {
            _this.pendingVerifications = _this.buildImageVerificationList(users);
        });
    };
    VerificationComponent.prototype.buildImageVerificationList = function (users) {
        return users.reduce(function (imageVerificationList, teacher) {
            var imageVerifications = teacher.imageEntryList
                .filter(function (imageEntry) { return imageEntry.isPending(); })
                .map(function (image) { return ({
                image: image,
                teacher: teacher
            }); });
            return imageVerificationList.concat(imageVerifications);
        }, []);
    };
    VerificationComponent.prototype.approveImage = function (user, image) {
        this.usersService.approveUserImage(user, image);
    };
    VerificationComponent.prototype.rejectImage = function (user, image) {
        this.usersService.rejectUserImage(user, image);
    };
    VerificationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-verification',
            template: __webpack_require__(/*! ./verification.component.html */ "./src/app/verification/verification.component.html"),
            styles: [__webpack_require__(/*! ./verification.component.css */ "./src/app/verification/verification.component.css")]
        }),
        __metadata("design:paramtypes", [_api_users_service__WEBPACK_IMPORTED_MODULE_1__["UsersService"]])
    ], VerificationComponent);
    return VerificationComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyDexifd7rRtogYdj25aD0MoOTWO3_Bfd2A',
        authDomain: 'attendance-f53f5.firebaseapp.com',
        databaseURL: 'https://attendance-f53f5.firebaseio.com',
        projectId: 'attendance-f53f5',
        storageBucket: 'attendance-f53f5.appspot.com',
        messagingSenderId: '487896345807'
    }
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Ciaran\Documents\dev\educam\app\educam-web\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map